import array
import math
import sys
import zlib

from Crypto.Random import random

from ecc import EC, ElGamal, Coord


def get_randomPerfSquare(n):
    limit = int(math.sqrt(n))
    m = limit - 1
    return math.pow(m, 2)


def get_nPrimeNumbers(n):
    prime_nums = []
    num = 2
    while len(prime_nums) < n:
        for i in range(2, num):
            if (num % i) == 0:
                break
        else:
            prime_nums.append(num)
        num += 1

    return prime_nums


def progress(process, progress):
    progress = int(progress)
    progress = process + " : " + str(progress) + " %"
    sys.stdout.write('\r' + progress)


class EllipticCrypto:
    def __init__(self, password):
        self.password = password
        self.a = None
        self.b = None
        self.p = None
        self.d = None
        self.eg = None
        self.prime_nums = None

    def prepare_curve_parameters(self):
        len_password = len(self.password)
        self.prime_nums = get_nPrimeNumbers(127 + len_password)

        P = self.prime_nums[-1]
        D = self.prime_nums[len_password]

        password_block = [ord(x) for x in self.password]
        if len_password % 2 != 0:
            password_block.append(len_password)
       
        half_len = len(password_block) / 2
        block1 = password_block[:half_len]
        block2 = password_block[half_len:]
       
        byte_array1 = array.array('B', block1)
        M = zlib.crc32(byte_array1) & 0xffffffff

        byte_array2 = array.array('B', block2)
        N = zlib.crc32(byte_array2) & 0xffffffff

        A = M % P
        B = N % P

        self.a = A
        self.b = B
        self.p = P
        self.d = D

    def get_cryptosystem(self):
        self.prepare_curve_parameters()
        ec = EC(self.a, self.b, self.p)

        rand_perf_square = get_randomPerfSquare(self.p)
        g, _ = ec.at(rand_perf_square)

        self.eg = ElGamal(ec, g)

    def encrypt(self, plain_text):
        try:
            self.get_cryptosystem()
        except Exception as e:
            return None, False
        if len(plain_text) % 2 != 0:
            plain_text += '$'
        plain_blocks = [Coord(ord(plain_text[i]), ord(plain_text[i + 1])) for i in range(0, len(plain_text), 2)]

        priv = self.d
        pub = self.eg.gen(priv)

        r = self.prime_nums[random.randint(0, len(self.prime_nums))]

        cipher = []
        pl_len = len(plain_blocks)
        i = 0
        for plain in plain_blocks:
            c = self.eg.enc(plain, pub, r)
            cipher.append(c)

            i += 1
            prog = (float(i) / pl_len) * 100
            progress("Encrypting", prog)

        print ('\n')
      
        return cipher, True

    def decrypt(self, cipher):
        try:
            self.get_cryptosystem()
        except Exception as e:
            return None, False
        priv = self.d
        plain = []
        cip_len = len(cipher)
        i = 0
        for c in cipher:
            d = self.eg.dec(c, priv)
            plain.append(d)
            i += 1
            prog = (float(i) / cip_len) * 100
            progress("Decrypting", prog)

        print ('\n')
        plain = map(lambda p: Coord(int(p.x), int(p.y)), plain)
       
        plain_text = ''
        for p in plain:
            try:
                plain_text += unichr(p.x) + unichr(p.y)
            except Exception as e:
                print (e)
                print (p)
                break

        if plain_text[-1] == '$':
            plain_text = plain_text[:-1]

        print(plain_text)

        return plain_text, True


if __name__ == '__main__':
    main()
