import binascii
import pickle
import sys

from PIL import Image

from EccCrc import EllipticCrypto

REORDERED_IMAGE_NAME = 'reordered.png'
DECRYPTED_IMAGE_NAME = 'decrypted.png'
ENCRYPTED_FILE_NAME = 'cipher.pk'
EXTENSION = 'png'


class CryptoSystem:
    def __init__(self, password):
        self.ec = EllipticCrypto(password)

    def im2hex(self, im_file):
        with open(im_file, "rb") as imageFile:
            img_str = binascii.hexlify(imageFile.read())
            return img_str

    def hex2im(self, img_str):
        unhex = binascii.unhexlify(img_str)
        return unhex

    def matrix_reorder(self, image, reverse=False):
        w, h = image.size
        result_image = image.copy()
        org_pix = image.load()
        result_px = result_image.load()
        p, q = 0, 0
        for i in range(w):
            for j in range(h):
                if i == j:
                    if not reverse:
                        result_px[p, q] = org_pix[i, j]
                    else:
                        result_px[i, j] = org_pix[p, q]
                    if q == h - 1:
                        q = 0
                        p += 1
                    else:
                        q += 1
        for j in range(h):
            for i in range(w):
                if j > i:
                    if not reverse:
                        result_px[p, q] = org_pix[i, j]
                    else:
                        result_px[i, j] = org_pix[p, q]
                    if q == h - 1:
                        q = 0
                        p += 1
                    else:
                        q += 1
        for j in reversed(range(h)):
            for i in reversed(range(w)):
                if j < i:
                    if not reverse:
                        result_px[p, q] = org_pix[i, j]
                    else:
                        result_px[i, j] = org_pix[p, q]
                    if q == h - 1:
                        q = 0
                        p += 1
                    else:
                        q += 1

        return result_image

    def encrypt(self, img_str):
        cipher, status = self.ec.encrypt(img_str)
        if not status:
            print ("Please enter another password")
        return cipher, status

    def decrypt(self, im_file):
        cipher = pickle.load(open(im_file, 'rb'))
        img_str, status = self.ec.decrypt(cipher)
        if not status:
            print ("Please enter another password")
        return img_str, status

    def encode(self):
        pass

    def decode(self):
        pass


def encrypt(image, password):
    crypto = CryptoSystem(password)
    image = Image.open(image).convert('L')
    reorderd = crypto.matrix_reorder(image)
    reorderd.save(REORDERED_IMAGE_NAME, EXTENSION)

    img_str = crypto.im2hex(REORDERED_IMAGE_NAME)
    cipher, status = crypto.encrypt(img_str)
    pickle.dump(cipher, open(ENCRYPTED_FILE_NAME, 'wb'))
    return status


def decrypt(enc_file, password):
    crypto = CryptoSystem(password)
    img_str, status = crypto.decrypt(enc_file)
    img_data = crypto.hex2im(img_str)
    with open(REORDERED_IMAGE_NAME, 'wb') as imageFile:
        imageFile.write(img_data)
        imageFile.flush()
        imageFile.close()
    reorderd = Image.open(REORDERED_IMAGE_NAME).convert('L')
    final_img = crypto.matrix_reorder(reorderd, True)
    final_img.save(DECRYPTED_IMAGE_NAME, EXTENSION)
    return status


def main():
    opt = sys.argv[1]
    im_file = sys.argv[2]

    password = raw_input("Enter password: ")

    if opt == 'encrypt':
        encrypt(im_file, password)
    else:
        decrypt(im_file, password)


if __name__ == '__main__':
    main()
